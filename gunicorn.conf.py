import multiprocessing
import os

bind = os.getenv('GUNICORN_BIND', '0.0.0.0:5000')
workers = os.getenv('GUNICORN_WORKERS', (multiprocessing.cpu_count() * 2 + 1))
preload_app = True
worker_class = 'sync'
user = 'root'
group = 'root'
# accesslog = '.logs/gunicorn.access.log'
# errorlog = '.logs/gunicorn.error.log'
loglevel = 'debug'
pidfile = './gunicorn.pid'
keepalive = os.getenv('GUNICORN_KEEPALIVE', 3)
