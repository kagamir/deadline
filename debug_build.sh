#!/bin/bash

name="ddl"

if [ "$1" = '--build' ]; then
    docker build -f docker/Dockerfile -t $name .
fi

docker-compose -f ./debug_docker-compose.yaml up -d --remove-orphans 

pwd=`pwd`
docker run -it --rm \
    -v "${pwd}:/app"  \
    -p "5000:5000" \
    --network ddl_net \
    $name bash

docker-compose -f ./debug_docker-compose.yaml down