from flask import Flask, redirect, render_template, request, make_response, session
from os import urandom
import re
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError
from itsdangerous import want_bytes
import models
from datetime import datetime, timedelta, date
import functools
# 获得成就：调库侠 #

app = Flask(__name__)
app.secret_key = urandom(64)
app.config['SESSION_TYPE'] = 'sqlalchemy'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=31)
app.config['SQLALCHEMY_POOL_RECYCLE'] = 1


# 如果符合规则返回真
def textCheck(*args):
    for i in args:
        if not re.match('^[0-9a-zA-Z]*$', i):
            return False
    return True


def login_check(want_login=False):
    def wrapper(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            id = session.get('id')
            if not id and not want_login:
                return redirect('login.php', 302)
            elif id and want_login:
                return redirect('ddl.php', 302)
            else:
                # with models.db_maker() as db:
                #     kwargs["db"] = db
                resault = func(*args, **kwargs)
                return resault
        return inner
    return wrapper


@app.route('/')
def index():
    return redirect('ddl.php', 302)


@app.route('/ddl.php')
@login_check()
def ddl():
    user_id = session.get('id')
    with models.db_maker() as db:
        records = db.query(models.Task).filter(models.Task.user_id == user_id).all()
        user_name = db.query(models.User).filter(models.User.id == user_id).one().username
        remaind_days = list()
        for row in records:
            remaind = (row.ddl_date - date.today()).days
            if remaind > 0:
                remaind_days.append(remaind)

    return render_template('ddl.html', remaindDays=remaind_days, user=user_name)


@app.route('/login.php')
@login_check(want_login=True)
def login():
    return render_template('login.html', info='')


@app.route('/login.php', methods=['POST'])
def dologin():
    req_user = request.form.get('user')
    req_passwd = request.form.get('passwd')
    with models.db_maker() as db:
        db_user = db.query(models.User).filter(models.User.username == req_user).one_or_none()
        try:
            if db_user:
                PasswordHasher().verify(db_user.password, want_bytes(req_passwd))
                session['id'] = db_user.id
            else:
                raise VerifyMismatchError
        except VerifyMismatchError:
            return render_template('login.html', info='Incorrect username or password.')

    return redirect('ddl.php', 302)


@app.route('/register.php')
@login_check(want_login=True)
def register():
    return render_template('register.html')


@app.route('/register.php', methods=['POST'])
def doRegister():
    req_user = request.form.get('user')
    if len(req_user) > 20:
        return 'oops'
    req_passwd = request.form.get('passwd')
    with models.db_maker() as db:
        row = db.query(models.User).filter(models.User.username == req_user).first()
        if not row:
            db_passwd = PasswordHasher().hash(req_passwd)
            new_user = models.User(username=req_user, password=db_passwd)
            db.add(new_user)

            the_user = db.query(models.User).filter(
                models.User.username == new_user.username
            ).one()
            session['id'] = the_user.id
            return redirect('ddl.php', 302)
        else:
            return render_template('register.html', info='用户名已被使用')


@app.route('/check-username.php')
def checkUname():
    req_user = request.args.get('user')
    with models.db_maker() as db:
        user_exist = db.query(models.User).filter(
            models.User.username == req_user
        ).first()
        if not user_exist:
            return 'ok'
        else:
            return 'oops'


@app.route('/logout.php')
def logout():
    session.pop('id', None)
    return redirect('ddl.php', 302)


@app.route('/schedule.php')
@login_check()
def schedule():
    user_id = session.get('id')
    with models.db_maker() as db:
        records = db.query(models.Task).filter(
            models.Task.user_id == user_id
        ).order_by(
            models.Task.ddl_date.asc()
        ).all()
        user_name = db.query(models.User).filter(models.User.id == user_id).one().username
        return render_template('schedule.html', tasks=records,  user=user_name)



@app.route('/schedule.php', methods=['POST'])
@login_check()
def postask():
    user_id = session.get('id')
    mod = request.form.get('mod')
    date = request.form.get('date')
    if not date or len(date) > 20:
        return 'oops'
    todo = request.form.get('todo')
    if len(todo) > 100:
        return 'oops'

    task_id = request.form.get('id')
    with models.db_maker() as db:
        if mod == 'delete':
            db.query(models.Task).filter(models.Task.id == task_id).delete()
        elif mod == 'addnew':
            new_task = models.Task(
                id=task_id,
                user_id=user_id,
                value=todo,
                ddl_date=date,
                enable=1
            )
            db.add(new_task)
        elif mod == 'edit':
            # TODO: 是否启用该任务
            task_id = request.form.get('id')
            edited = db.query(models.Task).filter(models.Task.id == task_id).one()
            edited.value = request.form.get('todo')
            edited.ddl_date = request.form.get('date')

    return redirect('schedule.php', 302)


if __name__ == "__main__":
    app.run(host="127.0.0.1", debug=True)
