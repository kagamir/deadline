from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, Column, String, Integer, TIMESTAMP, Boolean, CHAR, TEXT, DATE
from config.config import *
from contextlib import contextmanager

Base = declarative_base()

engine = create_engine(f'mysql+mysqlconnector://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_IPADDR}:3306/ddl')

DBSession = sessionmaker(bind=engine)

@contextmanager
def db_maker():
    db = DBSession()
    try:
        yield db
        db.commit()
    except:
        db.rollback()
        raise
    finally:
        db.close()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, nullable=False, primary_key=True,
                autoincrement=True, unique=True)
    username = Column(String(100), nullable=False, unique=True)
    password = Column(CHAR(100), nullable=False)
    # cookie = Column(String(100))


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(Integer, nullable=False, primary_key=True, autoincrement=True, unique=True)
    user_id = Column(Integer, nullable=False)
    value = Column(TEXT)
    ddl_date = Column(DATE, nullable=False)
    enable = Column(Boolean, nullable=False)


Base.metadata.create_all(engine)
